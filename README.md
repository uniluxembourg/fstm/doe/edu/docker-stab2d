# docker-stab2d

A dockerised version of 
[Stab2d](https://www.isd.uni-hannover.de/de/studium/software/), 
a neat education-oriented structural analysis software for
planar frame structures (developed at Leibniz University Hanover).

The main reason for having this Docker container is the fact that Stab2d
is a 32bit Windows-only application and therefore difficult to run
under virtualisation on newer 64bit systems.
The [Fluxbox](http://fluxbox.org/) window manager is used since it 
runs smoothly on `x86` (in contrast to Openbox).

The Windows version of Stab2d is available for download from the 
[website](https://www.isd.uni-hannover.de/fileadmin/isd/software_files/S2DInstall.exe) 
of the _Institut für Statik und Dynamik_ (copyright holder).

# Running the Docker container with Stab2d

Once you have [Docker](https://www.docker.com) installed and running 
on your operating system (macOS, Linux)
you can issue the following command in the terminal
```
docker run --rm --name stab2d -p 8080:8080 -p 5900:5900 zedhub/stab2d
```
This downloads the Docker image from the DockerHub registry and launches the
container under the name `stab2d` while forwarding the container ports 
`8080` and `5900` to your host machine (its `localhost` network interface).

The terminal will show textual logging information, which should be harmless.

# Accessing the Docker container with Stab2d

In order to access the graphical user interface (GUI) of Stab2d
we have to make use of one of the exposed ports.

## Using a VNC-based remote screen sharing application

On macOS you can use the default-installed app `Screen Sharing` and connect 
to `localhost:5900` (default password is `stab2d`).

<img src="stab2d-vnc.png" alt="access via VNC">

## Using a web browser

Open your web browser and navigate to `http://localhost:8080`
(default password is `stab2d`).

<img src="stab2d-browser.png" alt="access via Google Chrome">

# File exchange between Stab2d via filebrowser

The container runs _filebrowser_ and provides remote file access to 
the folder `/work` by navigating to `http://localhost:8081`.

For this the port 8081 has to be exposed to the host on startup 
of the container, e.g. with
```
docker run --rm --name stab2d -p 8080:8080 -p 8081:8081 zedhub/stab2d
```
The default username is `stab2d` and default password is `stab2d`.

# File exchange between Stab2d and host

You can share a folder in your host file system with the Docker container.
```
docker run --rm --name stab2d -p 8080:8080 -p 5900:5900 -v /Users/statiker/stab2d-files:/work/shared zedhub/stab2d
```
In the example above we share the folder `/Users/statiker/stab2d-files` 
on our host machine with the container at `/work/shared`.
The default working directory of container `docker-stab2d` is `/work` 
and the shared folder should be easy to find.

# Graphics at higher/lower resolution

One can adapt the resolution of the virtual screen by setting the 
`SCREEN` environment variable with the `-e` flag when starting the container
```
docker run --rm --name stab2d -p 8080:8080 -p 5900:5900 -e SCREEN="1920x1080" zedhub/stab2d
```

Access through the browser (e.g. via `http://localhost:8081`) supports
automatic rescaling of the work screen.
Use the [TurboVNC](https://github.com/TurboVNC/turbovnc) client when accessing
directly (e.g. via `localhost:5900`) to take advantage of automatic screen resizing
and much better VNC performance (image quality, speed).

# TODO

- printing is not configured
- build local image `stab2d` on Apple Silicon: 
  `docker build --progress plain --pull --no-cache -t stab2d .`
