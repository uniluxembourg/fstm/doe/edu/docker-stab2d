#! /usr/bin/env python3

import argparse
import subprocess
import random
import string

parser = argparse.ArgumentParser()

parser.add_argument('--verbose', action='store_true', help='Verbose output')
parser.add_argument('--num', type=int, default=1, help='Number of instances')
parser.add_argument('--cmd', type=str, default="docker", help='Docker command')
parser.add_argument('--img', type=str, default="registry.gitlab.com/uniluxembourg/fstm/doe/zed/tools/docker/docker-stab2d", help='Docker image')
parser.add_argument('--report', action='store_true', help='Prepare report')
parser.add_argument('--norandom', action='store_true', help='No randomised passwords')
parser.add_argument('--stop', action='store_true', help='Stop all instances')

args = parser.parse_args()
base = 'stab2d'
host = 'http://betti.uni.lux'

instance = []

if args.stop:
    r = subprocess.run(f'{args.cmd} container stop $({args.cmd} container ls -q --filter name={base})', shell=True, capture_output=True)
    if r.returncode == 1:
        print(r.stderr.decode())
    else:
        print(r.stdout.decode())
else:
    cmd = f'{args.cmd} pull {args.img}'
    print(cmd)
    subprocess.run(cmd, shell=True)

    for k in range(1, args.num + 1):

        vnc, web, dat = 7000 + k, 8000 + k, 9000 + k
        password = ''.join(random.choices(string.ascii_lowercase + string.digits, k=4))

        namek = f'{base}-{k:03d}-{password}'
        ports = f'-p {vnc}:5900 -p {web}:8080 -p {dat}:8081'
        evars = '' if args.norandom else f'-e PASSWORD={password}'

        cmd = f'{args.cmd} run --rm --name {namek} {ports} {evars} -d {args.img}'
        print(cmd)
        subprocess.run(cmd, shell=True)

        instance.append({'id': namek, 'password': password, 'url': f'{host}:{web}', 'files': f'{host}:{dat}'})

    if args.report:

        md = '| ' + ' | '.join(map(str, instance[0].keys())) + ' |' + '\n'
        md += '|-----' * len(instance[0].keys()) + '|' + '\n'
        md += ''.join(['| ' + ' | '.join(k.values()) + ' |' + '\n' for k in instance])

        print(md)

        with open('instances.md', 'w') as mdfile:
            mdfile.write(md)
