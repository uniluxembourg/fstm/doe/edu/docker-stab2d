FROM alpine AS build

ARG install_deps="wget cmake build-base libjpeg-turbo-dev libx11-dev libxext-dev"

ARG TVNC="https://github.com/TurboVNC/turbovnc/archive/refs/tags/3.1.3.tar.gz"
ARG FILE="https://github.com/filebrowser/filebrowser/releases/download/v2.31.2/linux-amd64-filebrowser.tar.gz"
ARG MONO="https://github.com/madewokherd/wine-mono/releases/download/wine-mono-9.4.0/wine-mono-9.4.0-x86.msi"

ARG STAB="https://www.isd.uni-hannover.de/fileadmin/isd/software_files/S2DInstall.exe"
ARG SEBB="https://www.isd.uni-hannover.de/fileadmin/isd/software_files/SEBInstall.exe"
ARG ROTA="https://www.isd.uni-hannover.de/fileadmin/isd/software_files/RInstall.exe"

RUN echo "x86" > /etc/apk/arch

RUN apk update --allow-untrusted \
    && \
    apk upgrade --allow-untrusted \
    && \
    apk add --allow-untrusted --no-cache ${install_deps}

RUN wget -q -O - ${TVNC} | tar xz -C /var/cache/apk \
    && \
    mkdir /var/cache/apk/tvnc-build && cd /var/cache/apk/tvnc-build \
    && \
    cmake -G"Unix Makefiles" \
        -DCMAKE_BUILD_TYPE=Release -DTVNC_BUILDVIEWER=off -DTVNC_USEPAM=off -DTVNC_USETLS=off \
        -DCMAKE_C_FLAGS="-D_GNU_SOURCE" \
        /var/cache/apk/turbovnc-*/ \
    && \
    make -j 4 \
    && \
    make install

RUN wget -q -O - ${FILE} | tar xz -C /usr/bin filebrowser
 
RUN mkdir -pv /usr/share/wine/mono \
    && \
    wget -P /usr/share/wine/mono ${MONO} 

RUN wget -q -O /var/cache/apk/stab2d.exe ${STAB} \
    && \
    unzip -d / /var/cache/apk/stab2d.exe \
    && \
    wget -q -O /var/cache/apk/sebbes.exe ${SEBB} \
    && \
    unzip -d / /var/cache/apk/sebbes.exe \
    && \
    wget -q -O /var/cache/apk/rotass.exe ${ROTA} \
    && \
    unzip -d / /var/cache/apk/rotass.exe


FROM alpine
LABEL maintainer="Andreas Zilian <andreas.zilian@uni.lu>"

COPY --from=build /opt/TurboVNC /opt/TurboVNC
COPY --from=build /usr/bin/filebrowser /usr/bin/filebrowser
COPY --from=build /usr/share/wine/mono /usr/share/wine/mono
COPY --from=build /stab2d /stab2d
COPY --from=build /sebbes /sebbes
COPY --from=build /rotass /rotass

ENV WINEPREFIX="/wine"
ENV SCREEN="1280x800"

ENV USERNAME="stab2d"
ENV PASSWORD="stab2d"

ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8

ARG runtime_deps="perl xinit xkbcomp xkeyboard-config xterm fluxbox font-overpass libturbojpeg supervisor novnc wine"

RUN echo "x86" > /etc/apk/arch

RUN apk update --allow-untrusted \
    && \
    apk upgrade --allow-untrusted \
    && \
    apk add --allow-untrusted --no-cache ${runtime_deps}

ENV PATH="/opt/TurboVNC/bin/:$PATH"

USER root

COPY etc /etc

RUN ln -s /usr/share/novnc/vnc.html /usr/share/novnc/index.html \
    && \
    ln -s /etc/fluxbox ~/.fluxbox \
    && \
    mkdir -p /work \
    && \
    ln -s /stab2d /work/stab2d \
    && \
    ln -s /sebbes /work/sebbes \
    && \
    ln -s /rotass /work/rotass \
    && \
    rm -rf /tmp/* /var/tmp/* /var/cache/apk/*

WORKDIR /work

EXPOSE 5900 8080 8081

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]
