#! /bin/sh

set -x

# vnc: prepare essential components (password),
#      run vncserver

USERNAME="${1:=stab2d}"
PASSWORD="${2:=stab2d}"
GEOMETRY="${3:=1280x800}"

# set password
mkdir -p ~/.vnc
echo ${PASSWORD} | vncpasswd -f > ~/.vnc/passwd
chmod 0600 ~/.vnc/passwd

# re-define xinitrc (to start window manager)
echo "exec fluxbox -no-slit" > /etc/X11/xinit/xinitrc

# run server
vncserver -name ${USERNAME} -geometry ${GEOMETRY} -rfbport 5900 -alwaysshared -fg :1
