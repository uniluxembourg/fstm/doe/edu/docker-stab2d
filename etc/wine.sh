#! /bin/sh

set -x

# wine: generate prefix
WINEDLLOVERRIDES="mscoree,mshtml=" wineboot --init >/dev/null 2>&1

echo "Generated wine prefix at WINEPREFIX=${WINEPREFIX}."
