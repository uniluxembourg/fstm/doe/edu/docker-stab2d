#! /bin/sh

set -x

# filebrowser: (re-)starting from scratch
# assumes to be executed in filebrowser working directory

USERNAME="${1:=stab2d}"
PASSWORD="${2:=stab2d}"

# make sure filebrowser is not running
killall filebrowser

# remove existing filebrowser database
rm -rf filebrowser.db

# config init (creates filebrowser.db)
filebrowser config init

# config settings
filebrowser config set \
	--address "0.0.0.0" \
	--port "8081" \
	--root "/work" \
	--baseurl "/" \
	--branding.name "Dateien (user=${USERNAME})" \
	--locale "de"

# add user with username and password
filebrowser users add ${USERNAME} ${PASSWORD}

# launch filebrowser
filebrowser
